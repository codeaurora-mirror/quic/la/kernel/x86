/*
 * Copyright (c)  2016 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicensen
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Authors: Sophia Gong <sophia.gong@intel.com>
 *
 */


#ifndef AUO39X39_CMD_H
#define AUO39X39_CMD_H

#include <drm/drmP.h>
#include <drm/drm.h>
#include <drm/drm_crtc.h>
#include <drm/drm_edid.h>
#include <asm/intel_scu_ipc.h>
#include "mdfld_output.h"
#include "mdfld_dsi_dpi.h"
#include "mdfld_dsi_pkg_sender.h"

/* In normal mode, panel scanning frequency is 60Hz */
#define idle_mode_off 0x38

/* In idle mode, panel scanning frequency is 30Hz */
#define idle_mode_on 0x39

static u8 frame_mem_control_column[] = {
	0x2a, 0x00, 0x04, 0x01, 0x89
};

static u8 frame_mem_control_row[] = {
	0x2b, 0x00, 0x00, 0x01, 0x85
};

void auo39x39_cmd_init(struct drm_device *dev, struct panel_funcs *p_funcs);
#endif
