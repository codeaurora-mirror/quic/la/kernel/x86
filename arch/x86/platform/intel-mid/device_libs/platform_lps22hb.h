/*
 * platform_tmd26723.h: TMD26723 proximity sensor platform data header file
 *
 * (C) Copyright 2016 Intel Corporation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GUN General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 */
#ifndef _PLATFORM_LPS22HB_H
#define _PLATFORM_LPS22HB_H
extern void *lps22hb_platform_data(void *info) __attribute__((weak));
#endif
