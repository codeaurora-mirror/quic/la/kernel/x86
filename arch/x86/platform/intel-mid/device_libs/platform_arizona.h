/*
 * platform_arizona.c: arizona platform data initilization file
 *
 * (C) Copyright 2016 Intel Corporation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 */
#ifndef _PLATFORM_ARIZONA_H_
#define _PLATFORM_ARIZONA_H_

extern void *arizona_platform_data(void *info) __attribute__((weak));
#endif
